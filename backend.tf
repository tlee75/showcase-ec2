# Store the state in a remote backend
terraform {
  backend "s3" {
    encrypt = true
  }
}